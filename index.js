class MyBigNumber {
	constructor(strn1, strn2) {
		this.stn1 = strn1;
		this.stn2 = strn2;
	}
	sum() {
		const strn1 = this.stn1;
		const strn2 = this.stn2;

		let remember = false;
		let result = [];

		for (let i = 0; i < strn2.length; i++) {
			let strn1Parse = Number(strn2[strn2.length - i - 1]);
			let strn2Parse = Number(strn1[strn1.length - i - 1]);

			let temp = strn1Parse + strn2Parse;

			if (remember) {
				temp++;
			}

			if (temp > 9) {
				remember = true;
			}

			temp = temp % 10;

			result[strn1.length - i] = temp;
		}

		for (let i = 0; i < strn1.length - strn2.length; i++) {
			let temp = Number(strn1[strn1.length - strn2.length - i - 1]);

			if (remember) {
				temp++;
			}

			if (temp > 9) {
				remember = temp;
			}

			temp = temp % 10;

			result[strn1.length - strn2.length - i] = temp;
		}

		return result;
	}
}

let myBigNumber = new MyBigNumber('1234', '897');
myBigNumber.sum();
