# Add 2 Numbers

# Các bước làm:

# Bước 1: Duyệt chuỗi 2

- Tính tổng từ trái qua phải bằng cách sử dụng đoạn code
- Lưu ý: chuyển về number
  let strn1Parse = Number(strn2[strn2.length - i - 1]);
  let strn2Parse = Number(strn1[strn1.length - i - 1]);
- Dùng biến tạm để lưu tổng của 2 giá trị này
- Có một biến remember nếu là true thì biến temp sẽ tăng lên 1 đơn vị
  VD: temp có giá trị lớn hơn hoặc bằng 10 thì gán remember = true ngược lại là false
- Để lấy chữ số hàng đơn vị của temp => temp % 10

# Bước 2: Số lần lặp là strn1.length - strn2.length

let temp = Number(strn1[strn1.length - strn2.length - i - 1]);
